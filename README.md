# Datetime List

日付、時間関係で `input:date` ではなく `select` を使わないといけないとき、`option` の候補を生成する。

## Example

JavaScript

```javascript
const list = DatetimeList.getHourMinuteList("09:00", "11:30", 30);
const options = list.map(datetime => `<option value="${datetime}">${datetime}</option>`);

document.createElement("select").innerHTML = options.join("");
```

生成される DOM

```html
<select>
  <option value="09:00">09:00</option>
  <option value="09:30">09:30</option>
  <option value="10:00">10:00</option>
  <option value="10:30">10:30</option>
  <option value="11:00">11:00</option>
  <option value="11:30">11:30</option>
</select>
```
