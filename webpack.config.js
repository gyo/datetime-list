const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

module.exports = env => {
  return {
    mode: env.production ? "production" : "development",
    entry: {
      index: "./src/js/index.js"
    },
    output: {
      filename: "[name].js",
      path: __dirname + "/dist/js"
    },
    plugins: [env.production ? new BundleAnalyzerPlugin() : false].filter(plugin => plugin),
    module: {
      rules: [
        {
          test: /\.js$/,
          loader: "babel-loader"
        }
      ]
    }
  };
};
