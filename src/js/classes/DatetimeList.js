/**
 *
 */
class DatetimeList {
  /**
   *
   * @param {string} headHourMinuteString 選択肢の開始時間（^-?\d{2}:\d{2}$）
   * @param {string} tailHourMinuteString 選択肢の終了時間（^-?\d{2}:\d{2}$）
   * @param {Number} intervalMinute 選択肢の間隔（分）
   * @returns {Array<String>} 選択肢のリスト
   */
  static getHourMinuteList(headHourMinuteString = "00:00", tailHourMinuteString = "23:59", intervalMinute = 60) {
    const result = [];
    const headMinuteNumber = DatetimeList.convertHourMinuteStringToBasedMinuteNumber(headHourMinuteString);
    const tailMinuteNumber = DatetimeList.convertHourMinuteStringToBasedMinuteNumber(tailHourMinuteString);

    let cachedMinuteNumber = headMinuteNumber;
    while (cachedMinuteNumber <= tailMinuteNumber) {
      result.push(DatetimeList.convertBasedMinuteNumberToHourMinuteString(cachedMinuteNumber));
      cachedMinuteNumber += intervalMinute;
    }

    return result;
  }

  /**
   *
   * @param {string} hourMinuteString 変換する文字列（^-?\d{2}:\d{2}$）
   * @returns {Number} "00:00" を 0 とした場合の経過分
   */
  static convertHourMinuteStringToBasedMinuteNumber(hourMinuteString) {
    const hoursAndMinutes = hourMinuteString.split(":");
    const hours = Number.parseInt(hoursAndMinutes[0], 10);
    const minutes = Number.parseInt(hoursAndMinutes[1], 10);

    return 60 * hours + minutes;
  }

  /**
   *
   * @param {Number} basedMinuteNumber "00:00" を 0 とした場合の経過分
   * @returns {string} （^-?\d{2}:\d{2}$）
   */
  static convertBasedMinuteNumberToHourMinuteString(basedMinuteNumber) {
    const hours = `${Math.floor(basedMinuteNumber / 60)}`.padStart(2, "0");
    const minutes = `${basedMinuteNumber % 60}`.padStart(2, "0");

    return `${hours}:${minutes}`;
  }
}

exports.DatetimeList = DatetimeList;
